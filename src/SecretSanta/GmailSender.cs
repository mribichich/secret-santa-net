namespace SecretSanta {
    using System;
    using System.IO;
    using System.Threading;

    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Gmail.v1;
    using Google.Apis.Gmail.v1.Data;
    using Google.Apis.Services;
    using Google.Apis.Util.Store;

    public class GmailSender {
        static readonly string[] Scopes = { GmailService.Scope.GmailSend };
        static readonly string ApplicationName = "Secret Santa";
        private static GmailService service;

        public static void Init(string dataDirectory) {
            UserCredential credential;

            using (var stream = new FileStream(Path.Combine(dataDirectory, "credentials.json"), FileMode.Open, FileAccess.Read)) {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                const string CredPath = "token.json";

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        Scopes,
                        "user",
                        CancellationToken.None,
                        new FileDataStore(CredPath, true))
                    .Result;

                Console.WriteLine("Credential file saved to: " + CredPath);
            }

            // Create Gmail API service.
            service = new GmailService(
                new BaseClientService.Initializer() { HttpClientInitializer = credential, ApplicationName = ApplicationName, });
        }

        public static Message CreateEmail(string from, string to, string subject, string body) {
            var mailMessage = new System.Net.Mail.MailMessage {
                From = new System.Net.Mail.MailAddress(@from), Subject = subject, Body = body, IsBodyHtml = true
            };
            mailMessage.To.Add(to);
            //mailMessage.ReplyToList.Add(email.FromAddress);

            var mimeMessage = MimeKit.MimeMessage.CreateFromMailMessage(mailMessage);

            return new Message { Raw = Encode(mimeMessage.ToString()) };
        }

        public static Message SendMessage(  Message email) {
            return service.Users.Messages.Send(email, "me").Execute();
        }

        public static string Encode(string text) {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(text);

            return System.Convert.ToBase64String(bytes).Replace('+', '-').Replace('/', '_').Replace("=", "");
        }
    }
}
